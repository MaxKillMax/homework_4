<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Администрирование</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
<header>
    <h1>Оптовая база товаров бытовой химии</h1>
</header>
<main>
    <h2>Администрирование</h2>
    <p class="mdc-typography--body1">Какой товар нужно добавить сегодня?</p>
    <a href="#" class="mdc-button mdc-button--raised">
        <span class="mdc-button__label">Добавить</span>
        <i class="material-icons mdc-button__icon" aria-hidden="true">add_circle_outline</i>
    </a>
</main>
<footer>
    <p>&copy; 2023 Оптовая база товаров бытовой химии</p>
</footer>
<script src="https://unpkg.com/material-components-web@13.0.0/dist/material-components-web.min.js"></script>
<script>
    mdc.autoInit();
</script>
</body>
</html>
