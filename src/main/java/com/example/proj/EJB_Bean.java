package com.example.proj;

import javax.ejb.*;
import javax.persistence.*;
import java.util.List;

@Stateless
public class EJB_Bean 
{

    @PersistenceContext(unitName = "myPersistenceUnit")
    private EntityManager em;

    public List<Bicycles> getAllEntities() 
	{
        TypedQuery<Bicycles> query = (TypedQuery<Bicycles>) em.createQuery("SELECT p FROM bicycles p");
        return query.getResultList();
    }

    public void addProduct(String name, String description, String link) 
	{
        Bicycles Bycicle = new Bicycles(name, description, link);
        em.persist(Bycicle);
    }

    public void removeProduct(String name) 
	{
        TypedQuery<Bicycles> query = (TypedQuery<Bicycles>) em.createQuery("SELECT p FROM bicycles p WHERE p.name=:name");
        query.setParameter("name", name);
        List<Bicycles> resultList = query.getResultList();
		
        for (Bicycles Bycicle : resultList) 
		{
            em.remove(Bycicle);
        }
    }
}
