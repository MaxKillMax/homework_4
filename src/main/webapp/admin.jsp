<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.*" %>
<%@ page import="com.example.proj.Bicycles" %>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.Context" %>
<%@ page import="com.example.proj.EJB_Bean" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Администрирование</title>
    <link rel="stylesheet" type="text/css" href="styles/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
<header>
    <h1>Прокат велосипедов</h1>
</header>
<main>
    <h2>Администрирование</h2>
    <p class="mdc-typography--body1">Какой товар нужно добавить?</p>
    <form method="POST">
        <input type="text" name="name" placeholder="Название">
        <input type="text" name="description" placeholder="Описание">
        <input type="text" name="link" placeholder="Ссылка на изображение">
        <button type="submit">Добавить</button>
    </form>
    <table class="mdc-data-table">
        <thead>
        <tr class="mdc-data-table__header-row">
            <th class="mdc-data-table__header-cell" role="columnheader" scope="col">Название</th>
            <th class="mdc-data-table__header-cell" role="columnheader" scope="col">Описание</th>
            <th class="mdc-data-table__header-cell" role="columnheader" scope="col">Изображение</th>
        </tr>
        </thead>
        <tbody class="mdc-data-table__content">
        <%!
            private EJB_Bean ejbBean;

            public void jspInit() {
                try {
                    Context context = new InitialContext();
                    ejbBean = (EJB_Bean) context.lookup("java:module/EJB_Bean");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        %>
        <%
            List<Bicycles> products = ejbBean.getAllEntities();
            for (Bicycles product : products) {
        %>
        <tr class="mdc-data-table__row">
            <td class="mdc-data-table__cell"><%= product.getName() %></td>
            <td class="mdc-data-table__cell"><%= product.getDescription() %></td>
            <td class="mdc-data-table__cell"><img src="<%= product.getUrl() %>" alt="<%= product.getName() %>"></td>
        </tr>
        <%
            }
        %>
        </tbody>
    </table>
</main>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
<script src="https://unpkg.com/material-components-web@13.0.0/dist/material-components-web.min.js"></script>
<script>
    mdc.autoInit();
</script>
</body>
</html>