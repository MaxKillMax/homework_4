<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <title>Прокат велосипедов</title>
    <link rel="stylesheet" type="text/css" href="styles/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
<header>
    <h1>Прокат велосипедов</h1>
</header>
<main>
    <h2>Вход</h2>
    <form action="auth" method="get">
        Имя:<input type="text" name="username"/><br/><br/>
        Пароль:<input type="password" name="userpass"/><br/><br/>
        <input type="submit" value="Войти"/>
    </form>
</main>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
<script src="https://unpkg.com/material-components-web@13.0.0/dist/material-components-web.min.js"></script>
<script>
    mdc.autoInit();
</script>
</body>
</html>